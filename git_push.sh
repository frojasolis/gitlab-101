#!/usr/bin/env bash

if [[ -z ${@} ]];then
    echo "No args provided"
    exit 1
fi

git add .

git commit -m "${@}"

git push

