const request = require('supertest');
const app = require('../server');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Hello Bootcamp Institute/, done);
  });
});
